export interface Prestamo {
  data:{
    fechaDevolucion:string;
    fechaPrestamo: string;
    libro:string;
    socio:string;
  }
}
