export interface Libro {

  data: {
    autor:string;
    edicion:string;
    editorial:string;
    lanzamiento: string;
    titulo: string;
    portada: string;
    prestado: boolean;

  }
}
