export interface Socio {
  data:{
    nombre:string;
    apellido: string;
    dni: string;
    fechaNacimiento: string;
    foto: string

  }
}
