import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {AngularFireModule} from "@angular/fire/compat";
import {environment} from "../environments/environment";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {LibroModule} from "./Componentes/Libro/libro.module";
import { NavbarComponent } from './Componentes/navbar/navbar.component';
import {SocioModule} from "./Componentes/socio/socio.module";
import {PrestamoModule} from "./Componentes/prestamo/prestamo.module";




@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    FormsModule,
    ReactiveFormsModule,
    LibroModule,
    SocioModule,
    PrestamoModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
