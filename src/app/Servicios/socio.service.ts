import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";

@Injectable({
  providedIn: 'root'
})
export class SocioService {

  constructor(private angularFirestore: AngularFirestore) { }
  //Crear un nuevo Socio

  public crearSocio(data: {nombre:string, apellido:string, dni:string, fechaNacimiento: string, foto: string }){

    return this.angularFirestore.collection('Socio').add(data);

  }

  //Obtiene un Socio

  public obtenerSocio(documentID: string){
    return this.angularFirestore.collection('Socio').doc(documentID).snapshotChanges();
  }

  //obtiene todos los Socios
  public obtenerSocios(){
    return this.angularFirestore.collection('Socio').snapshotChanges();
  }

  //Actualiza un Socio

  public actualizarSocio(documentID: string, data:any ){
    return this.angularFirestore.collection('Socio').doc(documentID).set(data);
  }

  public deleteSocio(documentID: string){

    return this.angularFirestore.collection('Socio').doc(documentID).delete();

  }

  public vaciarSocio(){
    nombre: '';
    apellido: '';
    dni: '';
    fechaNacimiento: '';
    foto: '';
    id: '';
  }

}
