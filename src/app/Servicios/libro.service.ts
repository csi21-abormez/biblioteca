import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";

@Injectable({
  providedIn: 'root'
})
export class LibroService {

  constructor(private angularFirestore: AngularFirestore){ }

  //Crear un nuevo Libro

  public crearLibro(data: {titulo:string, autor:string, edicion:string, editorial: string, lanzamiento: string }){

    return this.angularFirestore.collection('Libro').add(data);

  }

  //Obtiene un Libro

  public obtenerLibro(documentID: string){
    return this.angularFirestore.collection('Libro').doc(documentID).snapshotChanges();
  }

  //obtiene todos los libros
  public obtenerLibros(){
    return this.angularFirestore.collection('Libro').snapshotChanges();
  }

  //Actualiza un libro

  public actualizarLibro(documentID: string, data:any ){
    return this.angularFirestore.collection('Libro').doc(documentID).set(data);
  }

  public deleteLibro(documentID: string){

    return this.angularFirestore.collection('Libro').doc(documentID).delete();

  }



  public vaciarLibro(){
     titulo: '';
      autor: '';
      edicion: '';
      editorial: '';
      lanzamiento: '';
      id: '';
  }

  public obtenerLibrosSinPrestar(){
    return  this.angularFirestore.collection('Libro', ref =>ref.where ('prestado',"==",false )).snapshotChanges();
  }

  public actualizarPrestado(documentID: string, data:any ){
    return this.angularFirestore.collection('Libro').doc(documentID).set(data);
  }

}
