import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LibroComponent} from "./Componentes/Libro/libro.component";


const routes: Routes = [

  {path: 'Libro', loadChildren :() => import('./Componentes/Libro/libro.module').then(m => m.LibroModule)
},
  {
    path:'Prestamo', loadChildren : () => import('./Componentes/prestamo/prestamo.module').then(p => p.PrestamoModule)
  },

  {
    path:'Socio', loadChildren: () => import('./Componentes/socio/socio.module').then(s => s.SocioModule)
  },

  {
    path: '',
    redirectTo: 'Libro',
    pathMatch: 'full'
  },
  {
    path:'**',component: LibroComponent
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
