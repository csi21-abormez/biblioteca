import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarsocioComponent } from './editarsocio.component';

describe('EditarsocioComponent', () => {
  let component: EditarsocioComponent;
  let fixture: ComponentFixture<EditarsocioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarsocioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarsocioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
