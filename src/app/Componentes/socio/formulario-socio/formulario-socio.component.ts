import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {SocioService} from "../../../Servicios/socio.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-formulario-socio',
  templateUrl: './formulario-socio.component.html',
  styleUrls: ['./formulario-socio.component.css']
})
export class FormularioSocioComponent implements OnInit {
  public socio:any;
  public form: any;
  public documentId:any;
  public nuevoSocioFormulario = new FormGroup({
    id: new FormControl(''),
    nombre: new FormControl(''),
    apellido: new FormControl(''),
    dni: new FormControl(''),
    fechaNacimiento: new FormControl(''),
    foto: new FormControl(''),

  });

  constructor(private socioService: SocioService, private router:Router) {
    this.nuevoSocioFormulario.setValue({
      id:'',
      nombre:'',
      apellido: '',
      dni: '',
      fechaNacimiento:'',
      foto:'',
    })
  }

  ngOnInit(): void {
  }

  public nuevoSocio(form:any , documentId = this.documentId){
    let data = {
      nombre: form.nombre,
      apellido: form.apellido,
      dni: form.dni,
      fechaNacimiento: form.fechaNacimiento,
      foto: form.foto
    }
    this.socioService.crearSocio(data).then(() => {
      console.log('Documento creado correctamente');
      this.socioService.vaciarSocio();

    });
    (error:any) => {
      console.error(error);
    }
    this.router.navigateByUrl('Socio')
  }

  public editarSocio(documentId:any){
    let editarSuscribe = this.socioService.obtenerSocio(documentId).subscribe((socio:any) => {
      this.documentId = documentId;
      this.nuevoSocioFormulario.setValue({
        id: this.documentId,
        nombre: socio.payload.data()['nombre'],
        apellido: socio.payload.data()['apellido'],
        dni: socio.payload.data()['dni'],
        fechaNacimiento: socio.payload.data()['fechaNacimiento'],
        foto: socio.payload.data()['foto']
      })
    })
  }
}
