import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SocioComponent} from "./socio.component";
import {FormularioSocioComponent} from "./formulario-socio/formulario-socio.component";
import {EditarsocioComponent} from "./editarsocio/editarsocio.component";

const routes: Routes = [
  {path:'', component: SocioComponent },
  {path:'socioFormulario', component: FormularioSocioComponent },
  {path: 'editarSocio/:id', component: EditarsocioComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SocioRoutingModule { }
