import { Component, OnInit } from '@angular/core';
import {SocioService} from "../../Servicios/socio.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-socio',
  templateUrl: './socio.component.html',
  styleUrls: ['./socio.component.css']
})
export class SocioComponent implements OnInit {

  public socios: any = [];


  constructor(private socioService: SocioService, private router:Router) { }

  ngOnInit(): void {
    this.socioService.obtenerSocios().subscribe((socioSnapchot: any) => {
      this.socios = [];
      socioSnapchot.forEach((socioData: any) => {
        this.socios.push({
          id: socioData.payload.doc.id,
          data: socioData.payload.doc.data()
        });
      })
    })

  }

  public eliminarSocio(documentId: any){
    this.socioService.deleteSocio(documentId).then(() =>{
      console.log('Documento eliminado correctamente');

    },(error:any) =>{
      console.log(error);
      });
    this.router.navigateByUrl('Socio')
  }


}
