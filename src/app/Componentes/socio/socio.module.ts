import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SocioRoutingModule } from './socio-routing.module';
import {SocioComponent} from "./socio.component";
import { FormularioSocioComponent } from './formulario-socio/formulario-socio.component';
import {ReactiveFormsModule} from "@angular/forms";

import { EditarsocioComponent } from './editarsocio/editarsocio.component';


@NgModule({
  declarations: [SocioComponent, FormularioSocioComponent, EditarsocioComponent],
  imports: [
    CommonModule,
    SocioRoutingModule,
    ReactiveFormsModule
  ]
})
export class SocioModule { }
