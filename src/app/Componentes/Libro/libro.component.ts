import { Component, OnInit } from '@angular/core';
import {LibroService} from "../../Servicios/libro.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-libro',
  templateUrl: './libro.component.html',
  styleUrls: ['./libro.component.css']
})
export class LibroComponent implements OnInit {
  public libros: any = [];


  constructor(private libroService: LibroService, private router: Router) {

  }

  ngOnInit(): void {
    this.libroService.obtenerLibros().subscribe((librosSnapshot: any) => {
      this.libros = [];
      librosSnapshot.forEach((libroData: any) =>{
        this.libros.push({
          id: libroData.payload.doc.id,
          data: libroData.payload.doc.data()
        });
      });
      });
  }
  public eliminarLibro(libro: any){
    console.log(libro.data)
    if(libro.data.prestado == true){
      alert('El libro esta prestado y no se puede borrar')
    }else{
      this.libroService.deleteLibro(libro.id).then(()=>
      {
        console.log('Documento eliminado!');

      },(error:any) => {
        console.log(error);
      });
      this.router.navigateByUrl('Libro')
    }

  }




}
