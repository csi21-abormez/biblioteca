import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LibroService} from "../../../Servicios/libro.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {

libro?:any;
id:any;

  public nuevoLibroFormulario = new FormGroup({
    edicion: new FormControl('' ),
    autor: new FormControl('', Validators.required),
    editorial:new FormControl(''),
    lanzamiento: new FormControl(''),
    portada: new FormControl(''),
    titulo: new FormControl('',Validators.required ),
    id: new FormControl(''),


  });


  constructor(private libroService: LibroService, private activatedRoute: ActivatedRoute, private router:Router) {

  }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      this.id = <string>params.get('id');

      this.editLibro(this.id);

    });
  }

  public editLibro(form:any, id = this.id){
    let editSubscibe = this.libroService.obtenerLibro(id).subscribe((data:any) =>{
      this.nuevoLibroFormulario.setValue({
        id: this.id,
        titulo: data.payload.data()['titulo'],
        autor: data.payload.data()['autor'],
        edicion: data.payload.data()['edicion'],
        editorial: data.payload.data()['editorial'],
        portada: data.payload.data()['portada'],
        lanzamiento: data.payload.data()['lanzamiento'],

      });
      data = {
        titulo : form.titulo,
        autor: form.autor,
        edicion: form.edicion,
        editorial: form.editorial,
        lanzamiento: form.lanzamiento,
        portada: form.portada,
        prestado:false



      }
      this.libroService.actualizarLibro(id,data).then(()=>{
        alert("Documento actualizado correctamente");
      },(error) =>{
        console.log(error);
      });
      editSubscibe.unsubscribe();
      this.router.navigateByUrl('Libro')
    })


  }

  irAtras(){
    this.router.navigateByUrl('Libro')
  }
}
