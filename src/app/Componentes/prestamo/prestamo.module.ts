import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrestamoRoutingModule } from './prestamo-routing.module';
import {PrestamoComponent} from "./prestamo.component";
import { FormularioPrestamoComponent } from './formulario-prestamo/formulario-prestamo.component';
import {MatTabsModule} from '@angular/material/tabs';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { EditarprestamoComponent } from './InformacionPrestamo/editarprestamo.component';


@NgModule({
  declarations: [PrestamoComponent, FormularioPrestamoComponent, EditarprestamoComponent],
  imports: [
    CommonModule,
    PrestamoRoutingModule,
    MatTabsModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class PrestamoModule { }
