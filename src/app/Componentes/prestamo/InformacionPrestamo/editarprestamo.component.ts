import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {PrestamoService} from "../../../Servicios/prestamo.service";
import {LibroService} from "../../../Servicios/libro.service";
import {SocioService} from "../../../Servicios/socio.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-editarprestamo',
  templateUrl: './editarprestamo.component.html',
  styleUrls: ['./editarprestamo.component.css']
})
export class EditarprestamoComponent implements OnInit {
  prestamo?:any = {};
  id:any;
  socio:any = {};
  libro:any = {};

  constructor(private prestamoService: PrestamoService, private libroService: LibroService, private socioService: SocioService, private activatedRouter: ActivatedRoute, private router:Router) { }

  ngOnInit(): void {
    this.activatedRouter.paramMap.subscribe((params)=>{
      this.id = <string>params.get('id');
      let editSuscribe = this.prestamoService.obtenerPrestamo(this.id).subscribe((prestamo:any) =>{
        this.prestamo= {
          id: this.id,
          data: prestamo.payload.data(),
        }
        let idSocio = this.prestamo.data.socio;
        let idLibro = this.prestamo.data.libro;

        this.socioService.obtenerSocio(idSocio).subscribe((socio) =>{
          this.socio = {
            id: idSocio,
            data: socio.payload.data()
          }
        });

        this.libroService.obtenerLibro(idLibro).subscribe((libro)=>{
          this.libro = {
            id: idLibro,
            data: libro.payload.data(),
          }
          console.log(this.libro.data.titulo)
        })
        console.log(this.prestamo)
        editSuscribe.unsubscribe();
      });
    })
  }


  irAtras(){
    this.router.navigateByUrl('Prestamo')
  }

}
