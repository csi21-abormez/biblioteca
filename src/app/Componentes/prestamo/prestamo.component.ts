import { Component, OnInit } from '@angular/core';
import {PrestamoService} from "../../Servicios/prestamo.service";
import {LibroService} from "../../Servicios/libro.service";
import {SocioService} from "../../Servicios/socio.service";
import {Router} from "@angular/router";
import {Location} from "@angular/common";
import {timeout} from "rxjs/operators";

@Component({
  selector: 'app-prestamo',
  templateUrl: './prestamo.component.html',
  styleUrls: ['./prestamo.component.css']
})
export class PrestamoComponent implements OnInit {
  listaPrestamos: any[] = [];
  libro: any[]=[];
  socio: any[]= [];
  prestamo:any ={};
  libroauxiliar: any= {};



  constructor(private prestamoService: PrestamoService, private libroService: LibroService, private socioService: SocioService, private router: Router, private location: Location) {

    this.prestamoService.obtenerPrestamos().subscribe(prestamo =>{
      this.listaPrestamos = [];
      prestamo.forEach( prestamo => {
        this.listaPrestamos.push({
        id: prestamo.payload.doc.id,
          data: prestamo.payload.doc.data(),
        });
      });
    });
  }

  ngOnInit(): void {
    this.listaPrestamos = [];
  }

  public eliminarPrestamo(prestamo: any){
    let eliminarSuscribe = this.libroService.obtenerLibro(prestamo.data.libro).subscribe(libro => {
      this.libroauxiliar = {
        id: prestamo.data.libro,
        data: libro.payload.data()
      }

      console.log(this.libroauxiliar);
      this.libroauxiliar.data.prestado = false
      this.libroService.actualizarLibro(this.libroauxiliar.id, this.libroauxiliar.data).then(() => {this.prestamoService.deletePrestamo(prestamo.id)})
      alert('Documento eliminado correctamente');
      eliminarSuscribe.unsubscribe()
      this.router.navigateByUrl('Prestamo')

      console.log()



    }) ;

  }




  }
