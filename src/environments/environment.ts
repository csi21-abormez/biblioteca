// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCVuDjVBGKWZRLKyTqOofSeMsS5p60tPrc",
    authDomain: "gatos-6b3c8.firebaseapp.com",
    projectId: "gatos-6b3c8",
    storageBucket: "gatos-6b3c8.appspot.com",
    messagingSenderId: "844615789053",
    appId: "1:844615789053:web:5fae3796de2cdf7699d194"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
